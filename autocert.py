#!/usr/bin/python2
# -*- coding: utf8 -*-
########################################################################################
# This makes certificates under cert/ folder
# TODO: --update, --no-upload, [SUB FOLDER]
import subprocess, time, sys, os, copy, requests, json, uuid
print("autocert.py (requires names.txt and base.png)")

# Replace to UTF8
reload(sys)
sys.setdefaultencoding('utf-8')

try:
    if ("help" in sys.argv[1].lower()):
        raise Exception
    else:
        pass
    certid=0
    pname=sys.argv[1]
    ctime=sys.argv[2]
    if len(sys.argv) >= 4:
        codex=sys.argv[3]
        hashed=uuid.uuid1().hex+"-" # UUID1 keeps issuer
        print("Codex: %s" % codex)
        print("UUID: %s" % hashed)
        if len(sys.argv) == 5:
            certid=int(sys.argv[4])
    else:
        print("No Codex")
        codex=""
        reg=False
        hashed=""
except:
    print("./autocert.py \"ACTION\" \"LOAD\" [CODEX] [OFFSET]")
    print("Requires: base.png with the certificate PNG base")
    print("Requires: names.txt - listing of names")
    print("Optional: config.json for upload and signing (optional)")
    print("")
    print("ACTION - Name of the action being certified")
    print("LOAD - How much time was spent")
    print("CODEX - For registering, the prefix (eg. CA-A00)")
    print("OFFSET - For registering, changes where it begins")
    print("")
    print("Upload will only happen if CODEX is present")
    exit(0)

# Certificate configuration (edit it everytime)
if codex != "":
    print("Loading configuration file")
    f=open("config.json", "r")
    config={}
    cmap={"user", "pass", "server", "port", "download", "upload", "pass1", "pass2", "gpgkey"}
    reg=True
    try:
        config=json.load(f)
        if not all(key in config for key in cmap):
            print("Configuration file is invalid")
            raise
        reg=True
    except:
        print("Upload disabled")
        reg=False

    # FIXME
    config["folder"]="files"

# Stable settings
height=768
width=1024
namefont="Alex-Brush"
stdfont="Imperator"
color="black"
margin=125
pad1h=250 # first padding
fh=32 # font height
namefh=40 # name's font height
kerning=20 # space between text lines

# Setup
print("Creating certificates folder and emptying it")
subprocess.call("mkdir -p cert", shell=True)
subprocess.call("rm --recursive cert/*", shell=True)

# Create new variable: PERSONS
print("Reading names.txt (one name per line)")

f=open("names.txt", "r")
PERSONS=[]

for name in f:
    PERSONS.append(name.replace("\n", ""))

f.close()

# Declaration
def create_img(fontname, w, h, text):
    subprocess.call("""convert -pointsize %d -font "%s" -fill %s -annotate +%d+%d "%s" base.png tmp.png""" % (
                        fh, fontname, color, w, h, text), shell=True)
    #time.sleep(1)
    return True

def update_img(fontname, w, h, text, fh=fh):
    subprocess.call("""convert -pointsize %d -font "%s" -fill %s -annotate +%d+%d "%s" tmp.png tmp.png""" % (
                        fh, fontname, color, w, h, text), shell=True)
    #time.sleep(1)
    return True

def update_img2(fontname, w, h, text, fh=fh):
    cmd="""convert -pointsize %d -font "%s" -fill %s -gravity north -annotate +%d+%d "%s" tmp.png tmp.png""" % (
                              fh, fontname, color, w, h, text)
    print(cmd)
    subprocess.call(cmd, shell=True)
    #time.sleep(1)
    return True

def save_img(usr):
    subprocess.call("convert tmp.png cert/%s.webp" % (usr), shell=True)
    return True

# Keep the formatting consistent (from 99 to inf certificates)
wd=max(len(str(len(PERSONS))), 2)

# Create the images
print("Now rendering")
print("")
for me in PERSONS:
    certid+=1
    # Atualização
    #if certid != 8:
    #    continue
    fn=me.replace(" ", "_").replace(".", "").replace("\n", "")
    print("Writing for %s" % fn)
    h=0
    w=copy.copy(margin)
    # Write the header
    h+=pad1h
    create_img(stdfont, w, h, "Certificamos que")
    h+=fh+(kerning/2)
    # TODO: Centralize
    #update_img(namefont, w, h, me, fh=namefh)
    update_img2(namefont, 0, h, me, fh=namefh)
    h+=fh+(kerning*2.3)
    h=int(h)
    update_img(stdfont, w, h, "participou do(a) %s" % (pname))
    h+=fh+kerning
    update_img(stdfont, w, h, "Com carga horária de %s." % (ctime))
    if (codex != ""):
        h+=fh+kerning
        update_img(stdfont, w, h, "Verifique em %s" % (config["download"]))
        h+=fh+kerning
        update_img(stdfont, w, h, "Com o código %s%0*d" % (codex, wd, certid))
    save_img(fn)
    # Continue
    print("%s saved" % fn)
    # Send to remote server
    if (reg):
        subprocess.call("scp -4 -P %d cert/%s.webp %s/%s/%s-%s.webp" % (
            config["port"], fn, config["server"], config["folder"], hashed, fn), shell=True)
        # Sign and upload signed file
        print("Signing file...")
        subprocess.call("gpg2 -s -r %s cert/%s.webp" % (
            config["gpgkey"], fn), shell=True)
        # If signature exists, upload it
        if (os.path.exists("cert/%s.webp.gpg" % fn)):
            subprocess.call("scp -4 -P %d cert/%s.webp.gpg %s/%s/%s-%s.gpg" % (
                config["port"], fn, config["server"], config["folder"], hashed, fn), shell=True)
            signed="- <a href=%s/%s-%s.gpg>GPG Signature</a>" % (config["folder"], hashed, fn)
        else:
            print("SIGNATURE FAILED")
            signed=""
        # Register
        r = requests.post(config["upload"], data={
                "password": config["pass1"],
                "name": "%s%02d" % (codex, certid),
                "field": "<a href=%s/%s-%s.webp> %s </a>, %s (%s) %s" % (
                    config["folder"], hashed, fn, me, pname, ctime, signed),
                "hex": config["pass2"]},
            auth=(config["user"], config["pass"]))
        print("Returned %d\n%s" % (r.status_code,
            r.text.replace("<html>", "").replace("DOCTYPE", "")[:32]))
